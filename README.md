# Digtial Uhr 4 Digit mit Gehäuse
- Platine in Durchstecktechnik
- Level: Beginner 
- Zeit:  ~45 min
- SK Preis: 7 €

[http://www.banggood.com/de/DIY-0_8-Inch-Digital-Tube-LED-Electronic-Clock-Kit-Red-Blue-Green-LED-p-995712.html?rmmds=search](http://www.banggood.com/de/DIY-0_8-Inch-Digital-Tube-LED-Electronic-Clock-Kit-Red-Blue-Green-LED-p-995712.html?rmmds=search "Digital Uhr mit Gehäuse")

# Electronic Column LED Music Voice Spectrum Kit 

- Platine in SMD Technik
- Level: Fortgeschritten 
- Zeit: ~ ? min
- SK Preis 11 €


* Video Anleitung
[https://youtu.be/_ZXiFvHtxCk](https://youtu.be/_ZXiFvHtxCk)

[http://www.banggood.com/de/DIY-Dream-Crystal-Electronic-Column-Light-Cube-LED-Music-Voice-Spectrum-Kit-p-1078997.html?rmmds=search](http://www.banggood.com/de/DIY-Dream-Crystal-Electronic-Column-Light-Cube-LED-Music-Voice-Spectrum-Kit-p-1078997.html?rmmds=search)

# LED UHR 60LED Sekundenzeiger

- Videoanleitung [https://youtu.be/qzNTXYOEn4A](https://youtu.be/qzNTXYOEn4A)

[http://www.banggood.com/de/Upgrade-DIY-EC1515B-DS1302-Light-Control-Rotation-LED-Electronic-Clock-Kit-51-SCM-Learning-Board-p-1053190.html?rmmds=search](http://www.banggood.com/de/Upgrade-DIY-EC1515B-DS1302-Light-Control-Rotation-LED-Electronic-Clock-Kit-51-SCM-Learning-Board-p-1053190.html?rmmds=search)
